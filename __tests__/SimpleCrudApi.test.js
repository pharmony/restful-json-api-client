import fetchMock from 'fetch-mock'

import RestfulClient from '..'

const API_URL = 'https://api.domain.co/api'

let userApi
let userApiFetch

describe('RestfulClient CRUD', () => {
  beforeAll(() => {
    // In this test file, we don't care about the response
    fetchMock.mock('*', { status: 200 })

    class UsersApi extends RestfulClient {
      constructor () {
        super(API_URL, { resource: 'users', credentials: 'same-origin' })
      }
    }

    userApi = new UsersApi()
    userApiFetch = jest.spyOn(userApi, '_fetch')
  })

  afterAll(() => {
    fetchMock.reset()
  })

  describe('all()', () => {
    beforeEach(() => {
      userApi.all()
    })

    it('should call only one time the fetch function', () => {
      return expect(userApiFetch).toHaveBeenCalledTimes(1)
    })

    it('should send a GET request on the resource', () => {
      return expect(userApiFetch).toHaveBeenCalledWith(`${API_URL}/users`, {
        credentials: 'same-origin',
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json"
        },
        method: "GET"
      })
    })
  })

  describe('get()', () => {
    describe('by numerical ID', () => {
      beforeEach(() => {
        userApi.get({ id: 1 })
      })

      it('should call only one time the fetch function', () => {
        return expect(userApiFetch).toHaveBeenCalledTimes(1)
      })

      it('should send a GET request to the resource passing the ID', () => {
        return expect(userApiFetch).toHaveBeenCalledWith(`${API_URL}/users/1`, {
          credentials: 'same-origin',
          headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
          },
          method: "GET"
        })
      })
    })
    describe('by aphabetical ID', () => {
      beforeEach(() => {
        userApi.get({ id: 'Abc123' })
      })

      it('should call only one time the fetch function', () => {
        return expect(userApiFetch).toHaveBeenCalledTimes(1)
      })

      it('should send a GET request to the resource passing the ID', () => {
        return expect(userApiFetch).toHaveBeenCalledWith(
          `${API_URL}/users/Abc123`, {
          credentials: 'same-origin',
          headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
          },
          method: "GET"
        })
      })
    })
    describe('by query', () => {
      beforeEach(() => {
        userApi.get({ query: { username: 'zedtux' } })
      })

      it('should call only one time the fetch function', () => {
        return expect(userApiFetch).toHaveBeenCalledTimes(1)
      })

      it('should send a GET request to the resource passing the query', () => {
        return expect(userApiFetch).toHaveBeenCalledWith(
          `${API_URL}/users?username=zedtux`, {
            credentials: 'same-origin',
            headers: {
              "Accept": "application/json",
              "Content-Type": "application/json"
            },
            method: "GET"
          }
        )
      })
    })
  })

  describe('create()', () => {
    beforeEach(() => {
      userApi.create({ name: 'zedtux' })
    })

    it('should call only one time the fetch function', () => {
      return expect(userApiFetch).toHaveBeenCalledTimes(1)
    })

    it('should send a POST request to the resource', () => {
      return expect(userApiFetch).toHaveBeenCalledWith(`${API_URL}/users`, {
        credentials: 'same-origin',
        body: JSON.stringify({ name: 'zedtux' }),
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json"
        },
        method: "POST"
      })
    })
  })

  describe('update()', () => {
    beforeEach(() => {
      userApi.update(1, { name: 'Guillaume' })
    })

    it('should call only one time the fetch function', () => {
      return expect(userApiFetch).toHaveBeenCalledTimes(1)
    })

    it('should send a PATCH request to the resource passing its ID', () => {
      return expect(userApiFetch).toHaveBeenCalledWith(`${API_URL}/users/1`, {
        credentials: 'same-origin',
        body: JSON.stringify({ name: 'Guillaume' }),
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json"
        },
        method: "PATCH"
      })
    })
  })

  describe('destroy()', () => {
    beforeEach(() => {
      userApi.destroy(1)
    })

    it('should call only one time the fetch function', () => {
      return expect(userApiFetch).toHaveBeenCalledTimes(1)
    })

    it('should send a PATCH request to the resource passing its ID', () => {
      return expect(userApiFetch).toHaveBeenCalledWith(`${API_URL}/users/1`, {
        credentials: 'same-origin',
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json"
        },
        method: "DELETE"
      })
    })
  })
})
