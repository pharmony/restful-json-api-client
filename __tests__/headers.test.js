import fetchMock from 'fetch-mock'

import RestfulClient from '..'

// ~~~~ Config ~~~~
const API_URL = 'https://api.domain.co/api'

let sessionsApi
let sessionsApiFetch
let sessionsApiPromise

describe('RestfulClient Headers', () => {
  beforeAll(() => {
    fetchMock.post(`${API_URL}/sessions`, {
      body: JSON.stringify({
        token: 'this-is-your-token'
      }),
      status: 201
    })
  })

  afterAll(() => {
    fetchMock.reset()
  })

  describe('without passing any headers', () => {
    it('should accept JSON', () => {
      expect(new RestfulClient('https://d.co', {
        resource: 'users'
      }).headers).toMatchObject({ 'Accept': 'application/json' })
    })

    it('should set the Content-Type to JSON', () => {
      expect(new RestfulClient('https://d.co', {
        resource: 'users'
      }).headers).toMatchObject({ 'Content-Type': 'application/json' })
    })
  })

  describe('passing a custom header', () => {
    it('should merge the default headers and the custom one', () => {
      expect(new RestfulClient('https://d.co', {
        headers: {
          'X-my-custom': 'custom-value'
        },
        resource: 'users'
      }).headers).toEqual({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'X-my-custom': 'custom-value'
      })
    })
  })

  describe('redefining Content-Type', () => {
    it('should replace the default header', () => {
      expect(new RestfulClient('https://d.co', {
        headers: {
          'Content-Type': 'text/html'
        },
        resource: 'users'
      }).headers).toMatchObject({ 'Content-Type': 'text/html' })
    })
  })

  describe('disabling headers', () => {
    it('should empty the request headers', () => {
      expect(new RestfulClient('https://d.co', {
        headers: false,
        resource: 'users'
      }).headers).toEqual({})
    })
  })

  describe('configuring common headers', () => {
    beforeEach(() => {
      RestfulClient.configure({
        headers: {
          'X-RESTFUL-API': 'true'
        }
      })

      class SessionsApi extends RestfulClient {
        constructor () {
          super(API_URL, { resource: 'sessions' })
        }
      }

      sessionsApi = new SessionsApi()
      sessionsApiFetch = jest.spyOn(sessionsApi, '_fetch')
      sessionsApiPromise = sessionsApi.create()
    })

    afterEach(() => {
      sessionsApiFetch.mockRestore()
      RestfulClient.reset()
    })

    it('should have sent a POST request', () => (
      sessionsApiPromise.then(() => {
        expect(sessionsApiFetch).toHaveBeenCalledWith(`${API_URL}/sessions`, {
          body: JSON.stringify({}),
          headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "X-RESTFUL-API": "true"
          },
          method: "POST"
        })
      })
    ))
  })

  describe('using a function callback as header value', () => {
    const callbackFunction = () => (1 + 2 + 3 + 4 + 5)

    beforeEach(() => {
      RestfulClient.configure({
        headers: {
          'X-API-VERSION': () => '1.2.3',
          'X-RESTFUL-API': callbackFunction
        }
      })

      class SessionsApi extends RestfulClient {
        constructor () {
          super(API_URL, { resource: 'sessions' })
        }
      }

      sessionsApi = new SessionsApi()
      sessionsApiFetch = jest.spyOn(sessionsApi, '_fetch')
      sessionsApiPromise = sessionsApi.create()
    })

    afterEach(() => {
      sessionsApiFetch.mockRestore()
      RestfulClient.reset()
    })

    it('should use the callback function output as header value', () => (
      sessionsApiPromise.then(() => {
        expect(sessionsApiFetch).toHaveBeenCalledWith(`${API_URL}/sessions`, {
          body: JSON.stringify({}),
          headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "X-API-VERSION": "1.2.3",
            "X-RESTFUL-API": callbackFunction()
          },
          method: "POST"
        })
      })
    ))
  })
})
